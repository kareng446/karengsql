select *
from trader tr
join position p on tr.id = p.trader_ID
join trade t on p.opening_trade_ID = t.ID
join trade te on p.closing_trade_ID = te.ID
where p.trader_ID = 1 and t.stock = 'MRK';


select sum(t.size * t.price * (case when t.buy = 1 then -1 else 1 end))
from trade t
join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
where p.trader_id = 1
and t.stock = 'MRK'
and p.closing_trade_id != p.opening_trade_id
and p.closing_trade_id is not null;

  
create view traders_and_profits as
select
tr.first_name, tr.last_name,
sum(t.size * t.price * (case when t.buy = 1 then -1 else 1 end)) as 'profit'
from trade t
join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
join trader tr on p.trader_id = tr.id
where p.closing_trade_id is not null
and p.closing_trade_id != p.opening_trade_id
group by tr.first_name, tr.last_name;
