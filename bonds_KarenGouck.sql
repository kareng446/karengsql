select *
from bond
where CUSIP = '28717RH95';

select *
from bond
order by maturity asc;

select SUM(quantity * price) as total
from bond

select quantity * coupon/100 as totval
from bond

select *
from bond b
join rating r on r.rating = b.rating
where ordinal <= 3

select avg(price) as avgprice, avg(coupon) as avgcrate, rating
from bond
group by rating

select coupon/price as ratio, bond.rating, expected_yield
from bond
inner join rating
on bond.rating = rating.rating
where expected_yield > (coupon / price)